from django.http import FileResponse
from background_task import background
from django.shortcuts import render

import pika
import wget

def index(request):
    response = {}
    return render(request, 'home.html', response)

def progress(request):
    response = {}
    if request.POST:
        url = request.POST['url']
        downloadFromUrl(url)
    return render(request, 'progress.html', response)
    
@background(schedule=1)
def downloadFromUrl(url):
    filename = wget.download(url, bar=bar_custom)
    sendMessage('URL to download: <a>http://152.118.148.95:20457/download/?filename=' + filename + '</a>')

def bar_custom(current, total, width=80):
    message = "Downloading: %d%% [%d / %d] bytes" % (current / total * 100, current, total)
    print(message)
    sendMessage(message)

def sendMessage(message):
    routing_key = 'routingabcd12345'
    credentials = pika.PlainCredentials('0806444524', '0806444524')
    connectionParameters = pika.ConnectionParameters('152.118.148.95', 5672, '/0806444524', credentials)
    connection = pika.BlockingConnection(connectionParameters)
    channel = connection.channel()
    channel.exchange_declare(exchange='1606831035', exchange_type='direct')
    channel.queue_declare(queue='Routingabcd')
    channel.basic_publish(exchange='1606831035', routing_key=routing_key, body=message)
    print(" [x] Sent %r:%r" % (routing_key, message))
    connection.close()

def download(request):
    filename = request.GET['filename']
    file = open(filename, 'rb')
    return FileResponse(file)